﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmguCV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Bitmap image1 = new Bitmap(@"C:\VisualStudioProjects\EmguCV\EmguCV\bin\Debug\test3.png");
            //Bitmap image2 = new Bitmap(@"C:\VisualStudioProjects\EmguCV\EmguCV\bin\Debug\test4.png");
            Bitmap image1 = new Bitmap(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "stamp3_with_inner_stamp.jpg"));
            Bitmap image2 = new Bitmap(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "template3_clear.jpg"));

            Image<Bgr, byte> source = new Image<Bgr, byte>(image1); // Image B
            Image<Bgr, byte> template = new Image<Bgr, byte>(image2); // Image A
            Image<Bgr, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                System.Drawing.Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                if (maxValues[0] > 0.2)
                {
                    // This is a match. Do something with it, for example draw a rectangle around it.
                    System.Drawing.Rectangle match = new System.Drawing.Rectangle(maxLocations[0], template.Size);
                    imageToShow.Draw(match, new Bgr(System.Drawing.Color.Red), 3);
                }
            }

            // Show imageToShow in an ImageBox (here assumed to be called imageBox1)
            // Create the interop host control.
            System.Windows.Forms.Integration.WindowsFormsHost host =
                new System.Windows.Forms.Integration.WindowsFormsHost();

            ImageBox imageBox = new ImageBox();
            imageBox.Image = imageToShow;

            host.Child = imageBox;
            // Add the interop host control to the Grid
            // control's collection of child controls.
            container.Children.Add(host);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
